import React from 'react';
import 'antd/dist/antd.css';
import Routing from './router/Routing';

function App() {
  return (
    <div>
      <Routing />
    </div>
  );
}

export default App;
