import React, { useState } from 'react';
import { Button, Form, Input, Radio } from 'antd';
import { InfoCircleOutlined } from '@ant-design/icons';

function FourForm() {
  const [form] = Form.useForm();
  const [requiredMark, setRequiredMarkType] = useState('optional');
  const onRequiredTypeChange = ({ requiredMarkValue }) => {
    setRequiredMarkType(requiredMarkValue);
  };
  return (
    <Form
      form={form}
      layout="vertical"
      requiredMark={requiredMark}
      initialValues={{ requiredMarkValue: requiredMark }}
      onValuesChange={onRequiredTypeChange}
    >
      <Form.Item name="requiredMarkValue">
        <Radio.Group label="Required Mark" name="RequiredMark">
          <Radio.Button value="optional">optional</Radio.Button>
          <Radio.Button value>required</Radio.Button>
          <Radio.Button value={false}>hidden</Radio.Button>
        </Radio.Group>
      </Form.Item>
      <Form.Item label="Field A" required tooltip="this is a required field">
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item
        label="Field B"
        required
        tooltip={{ title: 'this is a required field', icon: <InfoCircleOutlined /> }}
      >
        <Input placeholder="input placeholder" />
      </Form.Item>
      <Form.Item>
        <Button type="primary"> Submit</Button>
      </Form.Item>
    </Form>
  );
}

export default FourForm;
