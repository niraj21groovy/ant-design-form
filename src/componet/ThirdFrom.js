import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Button, Radio } from 'antd';

function ThirdFrom() {
  const [form] = Form.useForm();
  const [formLayout, setFromLayout] = useState('harizontal');
  const onFormLayoutChange = ({ layout }) => {
    setFromLayout(layout);
  };
  const fromItemLayout =
    formLayout === 'horizontal'
      ? {
          lableCol: {
            span: 4,
          },
          wrapperCol: {
            span: 14,
          },
        }
      : null;

  const buttonItemLayout =
    formLayout === 'horizontal'
      ? {
          wrapperCol: {
            span: 14,
            offset: 4,
          },
        }
      : null;
  return (
    <div>
      <Form
        {...{ fromItemLayout }}
        form={form}
        onValuesChange={onFormLayoutChange}
        layout={formLayout}
        initialValues={{
          layout: formLayout,
        }}
      >
        <Form.Item label="Form layout" name="layout">
          <Radio.Group value={formLayout}>
            <Radio.Button value="horizontal">horizontal</Radio.Button>
            <Radio.Button value="vertical">vertical</Radio.Button>
            <Radio.Button value="inline">inline</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="field a">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item label="field b">
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item {...{ buttonItemLayout }}>
          <Button type="primary">submit</Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default ThirdFrom;
