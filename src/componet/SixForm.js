import { Input, Form, Button, Space, message } from 'antd';
import React from 'react';

function SixForm() {
  const [form] = Form.useForm();
  const onFill = () => {
    form.setFieldsValue({
      url: 'https://taobao.com/',
    });
  };
  const onFinsihFailed = () => {
    console.log('fail');
    message.error('submit fail');
  };
  const onFinish = () => {
    message.success('submit done');
  };
  return (
    <div>
      <Form
        form={form}
        layout="vertical"
        onFinish={onFinish}
        onFinishFailed={onFinsihFailed}
        autoComplete="off"
      >
        <Form.Item
          name="url"
          label="url"
          rules={[
            { required: true },
            {
              type: 'url',
              warningOnly: true,
            },
            {
              type: 'string',
              min: 6,
            },
          ]}
        >
          <Input placeholder="input placeholder" />
        </Form.Item>
        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              submit
            </Button>
            <Button htmlType="button" type="success" onClick={onFill}>
              fill
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  );
}

export default SixForm;
