import React from 'react';
import 'antd/dist/antd.css';
import { Button, Checkbox, Form, Input, InputNumber } from 'antd';

function index() {
  const OnFinish = (values) => {
    console.log('success:', values);
  };
  const OnFinishFailed = (errorInfo) => {
    console.log('faild:', errorInfo);
  };

  return (
    <Form
      name="basic"
      initialValues={{
        remember: true,
      }}
      wrapperCol={{
        span: 10,
      }}
      labelCol={{
        span: 8,
      }}
      OnFinish={OnFinish}
      requiredMark={false}
      OnFinishFailed={OnFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Username"
        name="Username"
        rules={[
          {
            required: true,
            message: 'please input your username',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="lastname"
        name="lastname"
        rules={[{ required: true, message: 'please input your last name' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="email"
        name="email"
        type="email"
        rules={[{ type: 'email', message: 'please input your last name' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Checkbox>remember</Checkbox>
      </Form.Item>
      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Form.Item
          name="number"
          rules={[
            {
              required: true,
              type: 'number',
              min: 0,
              max: 99,
              message: 'please input your last name',
            },
          ]}
        >
          <InputNumber />
        </Form.Item>
        <Button type="primary" htmlType="submit">
          button
        </Button>
      </Form.Item>
    </Form>
  );
}

export default index;
