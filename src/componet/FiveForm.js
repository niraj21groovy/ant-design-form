import React, { useState } from 'react';
import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Switch,
  TreeSelect,
} from 'antd';

function FiveForm() {
  const [componentSize, setComponentSize] = useState('default');
  const onFormLayoutChnage = ({ size }) => {
    setComponentSize(size);
  };
  return (
    <div>
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{
          span: 14,
        }}
        size={componentSize}
        initialValues={{
          size: componentSize,
        }}
        onValuesChange={onFormLayoutChnage}
      >
        <Form.Item label="form size" name="size">
          <Radio.Group>
            <Radio.Button value="small">small</Radio.Button>
            <Radio.Button value="default">default</Radio.Button>
            <Radio.Button value="large">large</Radio.Button>
          </Radio.Group>
        </Form.Item>
        <Form.Item label="Input">
          <Input />
        </Form.Item>
        <Form.Item label="Select">
          <Select>
            <Select.Option value="Demo">demo</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="TreeSelect">
          <TreeSelect
            treeData={[
              {
                title: 'Light',
                value: 'light',
                Children: [
                  {
                    title: 'Bamboo',
                    value: 'bamboo',
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        <Form.Item label="Cascader">
          <Cascader
            options={[
              {
                value: 'zhejiang',
                label: 'Zhejiang',
                Children: [
                  {
                    value: 'hangzhou',
                    lable: 'hangZhou',
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        <Form.Item label="Date Picker">
          <DatePicker />
        </Form.Item>
        <Form.Item label="InputNumber">
          <InputNumber />
        </Form.Item>
        <Form.Item label="Switch" valuePropName="checked">
          <Switch />
        </Form.Item>
        <Form.Item label="Button">
          <Button>Button</Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default FiveForm;
