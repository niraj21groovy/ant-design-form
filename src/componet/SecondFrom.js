import React from 'react';
import { Form, Input, Button, Select } from 'antd';

const { Option } = Select;
const layout = {
  lableCol: {
    span: 8,
  },
};
function SecondFrom() {
  const [form] = Form.useForm();
  const onChangeGender = (e) => {
    switch (e) {
      case 'male':
        form.setFieldsValue({
          note: 'hi, man',
        });
        return;
      case 'female':
        form.setFieldsValue({
          note: 'hi lady',
        });
        return;
      case 'other':
        form.setFieldsValue({
          note: 'hi there',
        });
        break;
      default:
        form.setFieldsValue({
          note: '',
        });
    }
  };
  const onFinish = (values) => {
    // eslint-disable-next-line no-console
    console.log(values);
  };
  const onReset = () => {
    form.resetFields();
  };
  const onFill = () => {
    form.setFieldsValue({
      note: 'hello world',
      gender: 'male',
    });
  };
  return (
    <Form form={form} {...{ layout }} requiredMark={false} onFinish={onFinish} name="control-hooks">
      <Form.Item
        label="note"
        name="note"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Gender"
        name="gender"
        rules={[
          {
            required: true,
          },
        ]}
      >
        <Select
          onChange={(e) => onChangeGender(e)}
          placeholder="Select a Option and changes input text above"
          allowClear
        >
          <Option value="male">male</Option>
          <Option value="female">female</Option>
          <Option value="other">other</Option>
        </Select>
      </Form.Item>
      <Form.Item
        noStyle
        shouldUpdate={(prevValues, currentValues) => prevValues.gender !== currentValues.gender}
      >
        {({ getFieldValue }) =>
          getFieldValue('gender') === 'other' ? (
            <Form.Item
              name="customizeGender"
              label="customize Gender"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input />
            </Form.Item>
          ) : null
        }
      </Form.Item>
      <Form.Item>
        <Button htmlType="submit">click here</Button>
        <Button htmlType="submit" onClick={onReset}>
          reset
        </Button>
        <Button type="link" onClick={onFill} htmlType="Button">
          fil form
        </Button>
      </Form.Item>
    </Form>
  );
}

export default SecondFrom;
