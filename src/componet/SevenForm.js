/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Input, Form, Button, message } from 'antd';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 20 },
  },
};

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offser: 0 },
    sm: { span: 20, offser: 4 },
  },
};

function SevenForm() {
  const onFinish = (values) => {
    message.success('Received values of form:');
    console.log(values);
  };
  return (
    <Form
      name="dynamic"
      {...{ formItemLayoutWithOutLabel }}
      onFinish={onFinish}
      initialValues={{ names: ['enter value'] }}
    >
      <Form.List
        name="names"
        rules={[
          {
            validator: async (item, names) => {
              if (!names || names.length < 1) {
                console.log(item);
                return Promise.reject(new Error('at least 2 passenager'));
              }
              return Promise.resolve();
            },
          },
        ]}
      >
        {(Fields, { add, remove }, { errors }) => (
          <div>
            {Fields.map((field, index) => (
              <Form.Item
                {...(index === 0
                  ? { ...{ formItemLayout } }
                  : { ...{ formItemLayoutWithOutLabel } })}
                label={index < 1 ? 'passenger' : ''}
                required={false}
                key={field.key}
              >
                <Form.Item
                  {...field}
                  validateTrigger={['onChnage', 'onBlur']}
                  rules={[
                    {
                      required: true,
                      whitespace: true,
                      message: 'please input passenger',
                    },
                  ]}
                  noStyle
                >
                  <Input placeholder="passenger name" style={{ width: '60%' }} />
                </Form.Item>
                {Fields.length > 1 ? (
                  <MinusCircleOutlined onClick={() => remove(field.name)} />
                ) : null}
              </Form.Item>
            ))}
            <Form.Item>
              <Button type="dashed" onClick={() => add()} icon={<PlusOutlined />}>
                add field
              </Button>
              <Button type="dashed" onClick={() => add('the head item', 0)} icon={<PlusOutlined />}>
                add field at head
              </Button>
              <Form.ErrorList errors={errors} />
            </Form.Item>
          </div>
        )}
      </Form.List>
      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
}

export default SevenForm;
