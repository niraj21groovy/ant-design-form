import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import SimpledFrom from '../componet/SimpleFrom';
import SecondFrom from '../componet/SecondFrom';
import ThirdFrom from '../componet/ThirdFrom';
import FourForm from '../componet/FourForm';
import FiveForm from '../componet/FiveForm';
import SixForm from '../componet/SixForm';
import SevenForm from '../componet/SevenForm';

function Routing() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/" component={SecondFrom} />
          <Route exact path="/2" component={SimpledFrom} />
          <Route exact path="/3" component={ThirdFrom} />
          <Route exact path="/4" component={FourForm} />
          <Route exact path="/5" component={FiveForm} />
          <Route exact path="/6" component={SixForm} />
          <Route exact path="/7" component={SevenForm} />
          <Redirect to="/" />
        </Switch>
      </Router>
    </div>
  );
}

export default Routing;
